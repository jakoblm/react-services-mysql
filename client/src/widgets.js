// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
import {Comment, Image, News, newsService} from "./services";

/**
 * Renders alert messages using Bootstrap classes.
 */
export class Alert extends Component {
  alerts: { id: number, text: React.Node, type: string }[] = [];
  static nextId = 0;

  render() {
    return (
      <>
        {this.alerts.map((alert, i) => (
          <div key={alert.id} className={'alert alert-' + alert.type} role="alert">
            {alert.text}
            <button
              type="button"
              className="close"
              onClick={() => {
                this.alerts.splice(i, 1);
              }}
            >
              &times;
            </button>
          </div>
        ))}
      </>
    );
  }

  static success(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ id: Alert.nextId++, text: text, type: 'success' });
    });
  }

  static info(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ id: Alert.nextId++, text: text, type: 'info' });
    });
  }

  static warning(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ id: Alert.nextId++, text: text, type: 'warning' });
    });
  }

  static danger(text: React.Node) {
    // To avoid 'Cannot update during an existing state transition' errors, run after current event through setTimeout
    setTimeout(() => {
      for (let instance of Alert.instances()) instance.alerts.push({ id: Alert.nextId++, text: text, type: 'danger' });
    });
  }
}

class NavBarLink extends Component<{ exact?: boolean, to: string, children?: React.Node }> {
  render() {
    return (
      <NavLink className="nav-link" activeClassName="active" exact={this.props.exact} to={this.props.to}>
        {this.props.children}
      </NavLink>
    );
  }
}

/**
 * Renders a navigation bar using Bootstrap classes
 */
export class NavBar extends Component<{ brand?: React.Node, children?: React.Node }> {
  static Link = NavBarLink;

  render() {
    return (
      <nav className="navbar navbar-expand-sm bg-light navbar-light">
        {
          <NavLink className="navbar-brand text-black-50" activeClassName="active" exact to="/">
            {this.props.brand}
          </NavLink>
        }
        <ul className="navbar-nav">{this.props.children}</ul>
      </nav>
    );
  }
}

/**
 * Renders an information card using Bootstrap classes
 */
export class Card extends Component<{ title: React.Node, children?: React.Node }> {
  render() {
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{this.props.title}</h5>
          <div className="card-text">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

/**
 * Renders a row using Bootstrap classes
 */
export class Row extends Component<{ children?: React.Node }> {
  render() {
    return <div className="row">{this.props.children}</div>;
  }
}

/**
 * Renders a column with specified width using Bootstrap classes
 */
export class Column extends Component<{ width?: number, right?: boolean, children?: React.Node }> {
  render() {
    return (
      <div
        className={'col' + (this.props.width ? '-' + this.props.width : '') + (this.props.right ? ' text-right' : '')}
      >
        {this.props.children}
      </div>
    );
  }
}

export class ButtonDanger extends Component<{
  onClick: () => mixed, // Any function
  children?: React.Node
}> {
  render() {
    return (
      <button type="button" className="btn btn-danger" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

export class ButtonSuccess extends Component<{
  onClick: () => mixed, // Any function
  children?: React.Node
}> {
  render() {
    return (
      <button type="button" className="btn btn-success" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

export class ButtonInfo extends Component<{
  onClick: () => mixed, // Any function
  children?: React.Node
}> {
  render() {
    return (
      <button type="button" className="btn bg-info" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}

/**
 * Renders a button using Bootstrap classes
 */
export class Button {
  static Danger = ButtonDanger;
  static Success = ButtonSuccess;
  static Info = ButtonInfo;
}

// box containing comments
export class CommentBox extends Component <{article_id: number}> {
  comments: Comment[] = [];
  newComment: Comment = new Comment();

  render() {
    return(
        <div className="card">
          <div className="card-header">
            Kommentarer
          </div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <input type="text" className="form-control" id="username" placeholder="brukernavn"
                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                         if (this.newComment) this.newComment.nickname = event.target.value;
                       }}

                />
              </div>
              <div className="form-group">
                <input type="text" className="form-control" id="content" placeholder="Kommentar"
                       onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                         if (this.newComment) this.newComment.comment = event.target.value;
                       }}
                />
              </div>
              <button onClick={this.postComment} className="btn btn-info">Send</button>
            </form>
          </div>
          <hr></hr>
          <Column >
            {this.comments.map(e =>{
              return(
                  <div style={{padding: "10px"}}>
                    <CardComment comment={e}/>
                  </div>
              )
            })}
          </Column>
        </div>
    )
  }
  mounted() {
    newsService
        .getComments(this.props.article_id)
        .then(comments => this.comments = comments)
        .catch((error: Error) => Alert.danger(error.message));
  }
  postComment() {
    this.newComment.article_id = this.props.article_id;
    newsService
        .postComment(this.newComment)
        .catch((error: Error) => Alert.danger(error.message));
    window.location.reload();
  }
}

// actual comment class
export class CardComment extends Component <{comment: Comment}> {
  render(){
    return(
        <div className="card" >
          <div className="card-header">
            @{this.props.comment.nickname}
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{this.props.comment.comment}</li>
          </ul>
        </div>
    );
  }
}

// card for frontpage
export class NewsCard extends Component <{news: News, image: Image}> {
  ne = this.props.news;
  im = this.props.image;

  render() {
    return (
        <a className="Card p-2 m-2 shadow text-dark bg-light" href={"#/article/"+ this.ne.article_id} style={{width: "47%"}}>
          <img src={this.im.image_url} className="card-img-top" alt={this.im.image_name} height="260px"/>
          <div className="card-body">
            <h5 className="card-title text-wrap">{this.ne.title}</h5>
            <p className="card-text text-wrap">{this.ne.header}</p>

          </div>
          <div className="card-footer">
            <small className="text-muted">Created {this.ne.created}</small>
          </div>
        </a>
    );
  }
}

// when there are no news on a page
export class EmptyPage extends Component {
  render() {
    return(
        <div>
          <p className="text-dark" style={{width: "400px",height: "100%", display: "flex", "justify-content": "center", "align-items": "center", "margin-top": "20%"}}>
            Ingen saker i denne kategorien
          </p>
        </div>
    )
  }
}

export class CenterColumn extends Component<{ children?: React.Node }>{
    render() {
        return(
            <div style={{width: "60%", margin: "auto", padding: "30px"}}>
                {this.props.children}
            </div>
        )
    }
}