// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert, NavBar, Card, Row, Column, Button, ButtonDanger, ButtonSuccess, ButtonInfo, NewsCard, CommentBox, EmptyPage, CenterColumn } from './widgets';
import { News, newsService, metadataService, Image, Category, Comment, } from './services';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import { Carousel } from 'react-bootstrap';
import { createHashHistory } from 'history';
const history = createHashHistory();
const LOGO = "https://i.imgur.com/PZt6qLL.png";
/*
    Meny på toppen av siden.
    Brukes til å navigere siden.
 */
class Menu extends Component {
    categories: Category[] = [];

    render() {
        return (
            <div className="navbar navbar-expand-md bg-light navbar-info" id="Main navgation bar">
                <NavBar>
                    <a href="/"><img src={LOGO} height={40} alt="Den lure vind"/></a>
                    <NavBar.Link exact to="/" >Forside</NavBar.Link>
                    <NavBar.Link exact to="/underholdning">underholdning</NavBar.Link>
                    {this.categories.map(e => (
                        <NavBar.Link exact to={"/tag/"+ e.category_id} key={e.category_id}>{e.description}</NavBar.Link>
                    ))}
                </NavBar>


                <div className="navbar-collapse collapse navbar-light">
                    <ul className="navbar-nav ml-auto">
                        <NavBar.Link exact to="/submit">Ny sak</NavBar.Link>
                    </ul>
                </div>
            </div>
        );
    }
    mounted() {
        metadataService
            .getCategories()
            .then(categories => (this.categories = categories))
            .catch((error: Error) => Alert.danger(error.message));
    }
}

/*
    Inneholder alle nyhetssakene på forsiden.
 */
class Frontpage extends Component{
    news: News[] = [];
    images: Image[] = [];

    render() {
        return (
            <div style={{width: "60%", margin: "auto"}}>
                <div className="card-deck">
                    {this.news.map(n => {
                        for(let i = 0; i<this.images.length; i++) {
                            if(this.images[i].image_id == n.image_id) {
                                return <NewsCard news={n} image={this.images[i]} key={n.article_id}/>
                            }
                        }
                    })}
                </div>
            </div>
        );
    }
    mounted() {
        newsService
            .getNews()
            .then(news => (this.news = news))
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getImages()
            .then(images => (this.images = images))
            .catch((error: Error) => Alert.danger(error.message));
    }
}
/*
    Newsfeed over forsiden som inneholder alle saker med viktighet 2.
 */
class Newsfeed extends Component{
    news: News[] = [];
    render() {
        return(
            <div style={{width: "60%", margin: "auto", padding: "20px"}} height={80}>
                <Row className="f-flex" style={{"max-height": "80px"}}>
                    <img className="" src="https://i.imgur.com/nTRUeHT.png" alt="Fast news!!!" height={80}/>
                    <Carousel className="flex-fill bg-light text-dark" style={{height: "80px", width: "70%"}} interval={1000} indicators={false} controls={false}>
                        {this.news.map(e => (
                                <Carousel.Item key={e.article_id}>
                                    <a href={"#/article/"+ e.article_id} style={{textAlign: "center"}}>
                                        <h3 className="text-dark">{e.title}</h3>
                                        <p className="text-dark">{e.created}</p>
                                    </a>
                                </Carousel.Item>
                            ))}
                    </Carousel>
                </Row>
            </div>
        )
    }
    mounted() {
        this.news = [];
        newsService
            .getFeed()
            .then(news => (this.news = news))
            .catch((error: Error) => Alert.danger(error.message));

        // oppdater hvert femte sekund
        setInterval(() => {
            newsService
                .getFeed()
                .then(news => (this.news = news))
                .catch((error: Error) => Alert.danger(error.message));
        }, 5000);
    }
}

/*
    Side som inneholder alle artikler med en gitt kategori.
 */
class Tag extends Component <{ match: { params: { id: number } } }>{
    news: News[] = [];
    images: Image[] = [];

    render() {
        return (
            <div style={{width: "60%", margin: "auto"}}>
                {(this.news.length > 0) ?
                    <div className="card-deck">
                        {this.news.map(n => {
                            for (let i = 0; i < this.images.length; i++) {
                                if (this.images[i].image_id == n.image_id) {
                                    return <NewsCard news={n} image={this.images[i]} key={n.article_id}/>
                                }
                            }
                        })}
                    </div>
                    :
                    <EmptyPage/>
                }
            </div>
        );
    }
    mounted() {
        this.news = [];
        newsService
            .getTag(this.props.match.params.id)
            .then(news => (this.news = news))
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getImages()
            .then(images => (this.images = images))
            .catch((error: Error) => Alert.danger(error.message));
    }
}

/*
    Underholdning siden med Trump sine tweets.
 */
class Trump extends Component {
    render() {
        return(
            <div style={{width: "50%", margin: "auto"}}>
                <TwitterTimelineEmbed
                    sourceType="profile"
                    screenName="realdonaldtrump"
                />
            </div>
        );
    }
}

/*
    Siden som viser en spesifik artikkel.
 */
class Article extends Component <{ match: { params: { id: number } } }> {
    news: News = new News('','', '', 0,0, 0);
    image: Image = new Image();
    category: Category = new Category();

    render() {
        if(!this.news) return null;
        return(
            <CenterColumn>
                <div className="card mb-3">
                    <img src={this.image.image_url} className="card-img-top" height="400px" alt={this.image.image_name}/>

                    <div className="card-body">
                        <h5 className="card-title">{this.news.title}</h5>
                        <h6>{this.news.header}</h6>
                        <p className="card-text">{this.news.content}</p>
                        <Row>
                            <Column>
                                <NavLink exact to={"/tag/"+ this.news.category_id}>{this.category.description}</NavLink>
                                <p className="card-text"><small className="text-muted">{this.news.created}</small></p>
                            </Column>
                            <button onClick={this.edit} className="btn btn-info" style={{ "margin-right": "15px"}}>Rediger</button>
                        </Row>
                    </div>
                </div>

                <CommentBox article_id={this.props.match.params.id}/>
            </CenterColumn>
        );
    }
    mounted() {
        let id = this.props.match.params.id;
        newsService
            .getArticle(id)
            .then(article => this.setup(article))
            .catch((error: Error) => Alert.danger(error.message));
    }
    setup(n: News){
        this.news = n;
        metadataService
            .getImage(this.news.image_id)
            .then(image => (this.image = image))
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getCategory(this.news.category_id)
            .then(category => (this.category = category))
            .catch((error: Error) => Alert.danger(error.message));
    }
    edit() {
        history.push('/update/'+ this.props.match.params.id)
    }
}

/*
    Inneholder skjemaet for å opprette en sak
 */
class Submit extends Component {
    images: Image[] = [];
    categories: Category[] = [];
    newNews: News = new News('','', '', 0,0, 0);
    selectedImage: Image = new Image();
    content: string = '';
    header: string = '';
    title: string = '';


    render() {
        return (
            <div className="bg-light" style={{padding: "20px", width: "60%", margin: "auto", "margin-top": "20px"}}>
                <form>

                    <div className="form-group">
                        <label>Tittel</label>
                        <input type="text" className="form-control" id="title" placeholder="tittel"
                           onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                               if (this.newNews) this.newNews.title = event.target.value;
                           }}
                        />
                    </div>

                    <div className="form-group">
                        <label>Innledning</label>
                        <input type="text" className="form-control" id="header" placeholder="innledning"
                           onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                               if (this.newNews) this.newNews.header = event.target.value;
                           }}
                        />
                    </div>

                    <div className="form-group">
                        <label>Velg kategori</label>
                        <select className="form-control" id="catSelector" onChange={this.selection}>
                            {this.categories.map(e => (
                                <option value={e.category_id}>{e.description}</option>
                            ))}
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Velg bilde</label>
                        <select className="form-control" id="imageSelector" onChange={this.selection}>
                            {this.images.map(e => (
                                <option value={e.image_id}>{e.image_name}</option>
                            ))}
                        </select>
                        <img style={{margin: "10px"}} height={"125px"} src={this.selectedImage.image_url} alt={this.selectedImage.image_name}/>
                    </div>

                    <div className="form-group">
                        <label>Velg viktighet</label>
                        <select className="form-control" id="importanceSelector" onChange={this.selection}>
                            <option value={1}>forside</option>
                            <option value={2}>newsfeed</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Innhold</label>
                        <textarea className="form-control" id="content" rows="6"
                              onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                  if (this.newNews) this.newNews.content = event.target.value;
                              }}
                        />
                    </div>

                    <Row>
                        <button onClick={this.post} className="btn btn-success" style={{ "margin-left": "1.3%"}}>Opprett</button>
                        <button onClick={this.cancel} className="btn btn-danger" style={{ "margin-left": "15px"}}>Avbryt</button>
                    </Row>
                </form>
            </div>
        );
    }
    mounted() {
        metadataService
            .getImages()
            .then(images => (this.initImage(images)))
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getCategories()
            .then(categories => (this.categories = categories))
            .catch((error: Error) => Alert.danger(error.message));
    }
    initImage(images: Image[]){
        this.images = images;
        this.selectedImage = images[0];
    }
    selection(){
        let category_id_element = document.getElementById('catSelector');
        let image_id_element  = document.getElementById('imageSelector');
        let importance_element = document.getElementById('importanceSelector');

        /*::
        if (!(category_id_element instanceof HTMLInputElement && importance_element instanceof HTMLInputElement && image_id_element instanceof HTMLInputElement)) {
            throw new Error('element is not of type HTMLInputElement');
        }*/

        this.newNews.category_id = Number(category_id_element.value);
        this.newNews.image_id = Number(image_id_element.value);
        this.newNews.importance = Number(importance_element.value);

        // find kan returnere void eller Image, må sjekke.
        const foo: void | Image = this.images.find((e: Image) => (e.image_id == this.newNews.image_id));
        if(foo) {
            this.selectedImage = foo;
        }


    }

    post() {
        if(this.title == '' || this.header == '' || this.content == '') {
            Alert.danger("Tomme felter");
            return;
        }
        this.selection();
        newsService
            .postArticle(this.newNews)
            .catch((error: Error) => Alert.danger(error.message));;
        history.push('/');
    }

    cancel() {
        history.push('/');
    }
}

/*
    skjemaet for å oppdatere en artikkel.
 */
class Update extends Component <{ match: { params: { id: number } } }> {
    news: News = new News('','', '', 0,0, 0);
    images: Image[] = [];
    categories: Category[] = [];
    selectedImage: Image = new Image();
    content: string = '';
    header: string = '';
    title: string = '';

    render() {
        return (
            <div className="bg-light" style={{padding: "20px", width: "60%", margin: "auto", "margin-top": "20px"}}>
                <form>

                    <div className="form-group">
                        <label>Tittel</label>
                        <input type="text" className="form-control" id="title" value={this.news.title}
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                   if (this.news) this.news.title = event.target.value;
                               }}
                        />
                    </div>

                    <div className="form-group">
                        <label>Innledning</label>
                        <input type="text" className="form-control" id="header" value={this.news.header}
                               onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                   if (this.news) this.news.header = event.target.value;
                               }}
                        />
                    </div>

                    <div className="form-group">
                        <label>Velg kategori</label>
                        <select className="form-control" id="catSelector" value={this.news.category_id} onChange={this.selection}>
                            {this.categories.map(e => (
                                <option value={e.category_id}>{e.description}</option>
                            ))}
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Velg bilde</label>
                        <select className="form-control" id="imageSelector" value={this.news.image_id} onChange={this.selection}>
                            {this.images.map(e => (
                                <option value={e.image_id}>{e.image_name}</option>
                            ))}
                        </select>
                        <img style={{margin: "10px"}} height={"125px"} src={this.selectedImage.image_url} alt={this.selectedImage.image_name}/>
                    </div>

                    <div className="form-group">
                        <label>Velg viktighet</label>
                        <select className="form-control" id="importanceSelector" value={this.news.importance} onChange={this.selection}>
                            <option value={1}>Forside</option>
                            <option value={2}>Nyhetsfeed</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label>Innhold</label>
                        <textarea className="form-control" id="content" rows="6" value={this.news.content}
                                  onChange={(event: SyntheticInputEvent<HTMLInputElement>) => {
                                      if (this.news) this.news.content = event.target.value;
                                  }}
                        />
                    </div>

                    <Row>
                        <button onClick={this.update} className="btn btn-success" style={{ "margin-left": "1.3%"}}>Oppdater</button>
                        <button onClick={this.cancel} className="btn btn-danger" style={{ "margin-left": "15px"}}>Avbryt</button>
                        <button onClick={this.delete} className="btn btn-danger" style={{ "margin-left": "15px"}}>Slett</button>
                    </Row>

                </form>
            </div>
        );
    }
    mounted() {
        newsService
            .getArticle(this.props.match.params.id)
            .then(article => this.news = article)
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getImages()
            .then(images => (this.initImage(images)))
            .catch((error: Error) => Alert.danger(error.message));
        metadataService
            .getCategories()
            .then(categories => (this.categories = categories))
            .catch((error: Error) => Alert.danger(error.message));
    }
    initImage(images: Image[]){
        this.images = images;
        this.selectedImage = images[0];
        this.selection();
    }
    selection(){
        let category_id_element = document.getElementById('catSelector');
        let image_id_element  = document.getElementById('imageSelector');
        let importance_element = document.getElementById('importanceSelector');

        /*::
        if (!(category_id_element instanceof HTMLInputElement && importance_element instanceof HTMLInputElement && image_id_element instanceof HTMLInputElement)) {
            throw new Error('element is not of type HTMLInputElement');
        }*/

        this.news.category_id = Number(category_id_element.value);
        this.news.image_id = Number(image_id_element.value);
        this.news.importance = Number(importance_element.value);

        // find kan returnere void eller Image, må sjekke.
        const foo: void | Image = this.images.find((e: Image) => (e.image_id == this.news.image_id));
        if(foo) {
            this.selectedImage = foo;
        }
    }
    update() {
        if(this.news.title == '' || this.news.header == '' || this.news.content == ''){
            Alert.danger("Tomme felter");
            return;
        }
        if(this.news.importance == null || this.news.image_id == null || this.news.category_id == null){
            return;
        }
        newsService
            .updateArticle(this.news)
            .then()
            .catch((error: Error) => Alert.danger(error.message));
        history.push("/article/"+ this.news.article_id)
    }
    delete() {
        newsService.deleteArticle(this.news.article_id)
        history.push('/');
    }
    cancel() {
        history.push('/');
    }
}

const root = document.getElementById('root');
if (root)
    ReactDOM.render(
        <HashRouter>
            <div className="bg-secondary">
                <Alert />
                <Menu />
                <Route exact path="/" component={Newsfeed} />
                <Route exact path="/" component={Frontpage} />
                <Route exact path="/article/:id" component={Article} />
                <Route exact path="/tag/:id" component={Tag} />
                <Route exact path="/submit" component={Submit} />
                <Route exact path="/update/:id" component={Update} />
                <Route exact path="/underholdning" component={Trump} />
            </div>
        </HashRouter>,
    root
);
