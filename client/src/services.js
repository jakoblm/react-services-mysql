// @flow
import axios from 'axios';

export class News {
	article_id: number;
	title: string;
	header: string;
	content: string;
	image_id: number;
	upvotes: number;
	downvotes: number;
	category_id: number;
	importance: number;
	created: string;

	constructor(title: string, header: string, content: string, image_id: number, category_id: number, importance: number){
        this.title = title;
        this.header = header;
        this.content = content;
        this.image_id = image_id;
        this.category_id = category_id;
        this.importance = importance;
	}
}

export class Comment {
	comment_id: number;
	article_id: number;
	nickname: string;
	comment: string;
}

/*
	Service klasse for å hente og behandle artikler
 */
class NewsService {

	getNews() {
		return axios.get<News[]>('/news').then(response => response.data);
	}

	getArticle(id: number) {
		return axios.get<News>('/article/' + id).then(response => response.data);
	}

	getFeed() {
		return axios.get<News[]>('/feed').then(response => response.data);
	}

	getTag(id: number) {
		return axios.get<News[]>('/tag/' + id).then(response => response.data);
	}

	postArticle(article: News) {
		return axios.post<News, void>('/submit', article).then(response => response.data);
	}

	updateArticle(article: News) {
		return axios.put<News, void>('/update', article).then(response => response.data);
	}

	deleteArticle(id: number){
		axios.delete<void>('/deleteComments/'+ id).then(response => response.data);
		axios.delete<void>('/delete/'+ id).then(response => response.data);
	}

	getComments(article_id: number){
		return axios.get<Comment[]>('/comments/' + article_id).then(response => response.data);
	}

	postComment(comment: Comment) {
		return axios.post<Comment, void>('/comment', comment).then(response => response.data);
	}
}
export let newsService = new NewsService();

export class Image {
	image_id: number;
	image_url: string;
	image_name: string;
}

export class Category {
	category_id: number;
	description: string;
}

/*
	Service klasse for å håndtere og behandle bilder og kategorier
 */
export class MetaService {

	getImages() {
		return axios.get<Image[]>('/images').then(response => response.data);
	}

	getImage(id: number) {
		return axios.get<Image>('/images/' + id).then(response => response.data);
	}

	getCategories() {
		return axios.get<Category[]>('/categories').then(response => response.data);
	}

	getCategory(id: number) {
		return axios.get<Category>('/categories/' + id).then(response => response.data);
	}
}

export let metadataService = new MetaService();