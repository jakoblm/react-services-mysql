import * as React from 'react';
import { Component } from 'react-simplified';
import { shallow} from "enzyme";
import { Image, News, newsService} from "../src/services.js";
import { NewsCard } from "../src/widgets";


describe('Article tests', () => {
    let news: News = new News(
        'tittel',
        'innledning',
        'innhold',
        1,
        1,
        1
        );

    let img: Image = new Image(1, 'bilde1', 'bildeURL');

    let card: NewsCard = new NewsCard({news, img});

    //const a = jest.spyOn(newsService, "getArticle").mockResolvedValue(news);
    //const wrapper = shallow( card );

    it('get article test', () => {
        let instance = card.instance();
        expect(typeof instance).toEqual('object');
        expect(card.ne.title).toMatch('titel');
        expect(card.ne.content).toMatch('innhold');
        expect(card.ne.header).toMatch('innledning');
        expect(card.ne.category).toMatch(1);
        expect(card.ne.image_id).toMatch(1);
        expect(card.ne.importance).toMatch(1);
    });
});
