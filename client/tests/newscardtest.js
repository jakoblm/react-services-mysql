import * as React from 'react';
import { Component } from 'react-simplified';
import { shallow, mount } from 'enzyme';
import { News, Image, Category, Comment} from '../src/services.js';
import {Alert, NewsCard} from "../src/widgets";

describe('NewsCard tests', () => {
    const wrapper = shallow(<NewsCard />);

    it('initially', () => {
        let instance = NewsCard.instance();
        expect(typeof instance).toEqual('object');
    });

    it('after load', done => {
        let news: News = new News('Overskrift', 'innledning', 'innhold',1,1,1);
        let card: NewsCard = new NewsCard();
        card.ne = news;

        setTimeout(() => {
            let instance = card.instance();
            expect(typeof instance).toEqual('object');
            done();
        });
    });
});
