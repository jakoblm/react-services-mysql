const Dao = require("./dao.js");

module.exports = class NewsDao extends Dao {

	getArticlesByImportance(importance, callback) {
		super.query(
			"SELECT * FROM articles WHERE importance = ? ORDER BY article_id DESC;",
			[importance],
			callback
		);
	}

	getArticlesByImportanceAndLimit(importance, limit, callback) {
		super.query(
			"SELECT * FROM articles WHERE importance = ? ORDER BY article_id DESC LIMIT ?;",
			[importance, limit],
			callback
		);
	}

	getArticle(id, callback) {
		super.query(
			"SELECT * FROM articles WHERE article_id = ?;",
			[id],
			callback
		);
	}

	getArticlesByCategory(category_id, callback) {
		super.query(
			"SELECT * FROM articles WHERE category_id = ? ORDER BY article_id DESC;",
			[category_id],
			callback
		);
	}

	getImages(callback) {
		super.query(
			"SELECT * FROM images;",
			[],
			callback
		);
	}

	getImageById(id, callback) {
		super.query(
			"SELECT * FROM images WHERE image_id = ?;",
			[id],
			callback
		);
	}

	getCategories(callback) {
		super.query(
			"SELECT * FROM category;",
			[],
			callback
		);
	}

	getCategoryById(id, callback) {
		super.query(
			"SELECT * FROM category WHERE category_id = ?;",
			[id],
			callback
		);
	}

	SubmitArticle(json, callback) {
		super.query(
			"INSERT INTO articles VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, DEFAULT);",
			[json.title, json.header, json.content, json.image_id, 10, 10, json.category_id, json.importance],
			callback
		);
	}

	UpdateArticle(json, callback) {
		super.query(
			"UPDATE articles SET title=?,header=?,content=?,image_id=?,category_id=?,importance=? WHERE article_id = ?;",
			[json.title, json.header, json.content, json.image_id, json.category_id, json.importance, json.article_id],
			callback
		);
	}

	deleteComments(id, callback) {
		super.query(
			"DELETE FROM comments WHERE article_id = ?;",
			[id],
			callback
		);
	}

	deleteArticle(id, callback) {
		super.query(
			"DELETE FROM articles WHERE article_id = ?;",
			[id],
			callback
		);
	}

	postComment(json, callback) {
		super.query(
			"INSERT INTO comments VALUES(DEFAULT, ?, ?, ?);",
			[json.article_id, json.nickname, json.comment],
			callback
		);
	}

	getComments(id, callback) {
		super.query(
			"SELECT * FROM comments WHERE article_id = ? ORDER BY comment_id DESC;",
			[id],
			callback
		);
	}
};
