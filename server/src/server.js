// @flow

import express from 'express';
import path from 'path';
import reload from 'reload';
import fs from 'fs';
import mysql from 'mysql';

export class News {
	article_id: number;
	title: string;
	header: string;
	content: string;
	image_id: number;
	upvotes: number;
	downvotes: number;
	category_id: number;
	importance: number;
	created: string;
}

export class Image {
	image_id: number;
	image_url: string;
	image_name: string;
}

export class Category {
	category_id: number;
	description: string;
}

export class Comment {
	comment_id: number;
	article_id: number;
	nickname: string;
	comment: string;
}

let pool = mysql.createPool({
	host: 'mysql-ait.stud.idi.ntnu.no',
	user: 'jakoblm',
	password: 'p1uvcygs',
	database: 'jakoblm',
	dateStrings: true
});

const NewsDao = require("./dao/NewsDao.js");
let newsDao = new NewsDao(pool);

const public_path = path.join(__dirname, '/../../client/public');
let app = express();

app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

// Forside nyheter er begrenset til 20 saker.
app.get('/news', (req: express$Request, res: express$Response) => {

	newsDao.getArticlesByImportanceAndLimit(1, 20, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

// Nyhetsfeed nyheter er begrenset til 3 saker.
app.get('/feed', (req: express$Request, res: express$Response) => {

	newsDao.getArticlesByImportanceAndLimit(2, 3, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.get('/article/:id', (req: express$Request, res: express$Response) => {

	newsDao.getArticle(req.params.id, (status, data) => {
		res.status(status);
		res.json(data[0]);
	});
});

app.get('/tag/:id', (req: express$Request, res: express$Response) => {

	newsDao.getArticlesByCategory(req.params.id, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.get('/images', (req: express$Request, res: express$Response) => {

	newsDao.getImages((status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.get('/images/:id', (req: express$Request, res: express$Response) => {

	newsDao.getImageById(req.params.id, (status, data) => {
		res.status(status);
		res.json(data[0]);
	});
});

app.get('/categories', (req: express$Request, res: express$Response) => {

	newsDao.getCategories((status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.get('/categories/:id', (req: express$Request, res: express$Response) => {

	newsDao.getCategoryById(req.params.id, (status, data) => {
		res.status(status);
		res.json(data[0]);
	});
});

app.post('/submit', (req: { body: News }, res: express$Response) => {

	newsDao.SubmitArticle(req.body, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.put('/update', (req: { body: News }, res: express$Response) => {

	newsDao.UpdateArticle(req.body, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.delete('/deleteComments/:id', (req: express$Request, res: express$Response) => {

	newsDao.deleteComments(req.params.id, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.delete('/delete/:id', (req: express$Request, res: express$Response) => {

	newsDao.deleteArticle(req.params.id, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.post('/comment', (req: { body: Comment }, res: express$Response) => {

	newsDao.postComment(req.body, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

app.get('/comments/:id', (req: express$Request, res: express$Response) => {

	newsDao.getComments(req.params.id, (status, data) => {
		res.status(status);
		res.json(data);
	});
});

export let listen = new Promise<void>((resolve, reject) => {
	reload(app).then(reloader => {
		app.listen(3000, (error: ?Error) => {
			if (error) reject(error.message);
			console.log('Express server started');
			reloader.reload();
			fs.watch(public_path, () => reloader.reload());
			resolve();
		});
	});
});
