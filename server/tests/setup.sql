DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS images;

CREATE TABLE images(
       image_id INTEGER AUTO_INCREMENT,
       PRIMARY KEY(image_id),
       image_name VARCHAR(30) NOT NULL,
       image_url VARCHAR(200) NOT NULL
);

CREATE TABLE category(
     category_id INTEGER AUTO_INCREMENT,
     PRIMARY KEY(category_id),
     description VARCHAR(40)
);

CREATE TABLE articles(
     article_id INTEGER AUTO_INCREMENT,
     PRIMARY KEY(article_id),
     title VARCHAR(40) NOT NULL,
     header VARCHAR(150) NOT NULL,
     content VARCHAR(1500) NOT NULL,
     image_id INTEGER NOT NULL,
     FOREIGN KEY(image_id) REFERENCES images(image_id),
     upvotes INTEGER UNSIGNED NOT NULL,
     downvotes INTEGER UNSIGNED NOT NULL,
     category_id INTEGER NOT NULL,
     FOREIGN KEY(category_id) REFERENCES category(category_id),
     importance INTEGER NOT NULL,
     created DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE comments(
     comment_id INTEGER AUTO_INCREMENT,
     PRIMARY KEY(comment_id),
     article_id INTEGER NOT NULL,
     FOREIGN KEY(article_id) REFERENCES articles(article_id),
     nickname VARCHAR(20) NOT NULL,
     comment VARCHAR(150) NOT NULL
);

DELETE FROM comments;
DELETE FROM articles;
DELETE FROM category;
DELETE FROM images;

INSERT INTO images VALUES (DEFAULT, 'Joker faller', 'https://66.media.tumblr.com/a01f1cf2bf8d624b0cd96a34aedbe0fc/d86d328e02e7f88f-8b/s1280x1920/4c982c9daaed3eab50f4723d171966f720999c15.jpg');
INSERT INTO images VALUES (DEFAULT, 'fakenews', 'https://ichef.bbci.co.uk/images/ic/720x405/p05vtkdr.jpg');
INSERT INTO images VALUES (DEFAULT, 'trump', 'http://d279m997dpfwgl.cloudfront.net/wp/2019/07/AP_19201004713022-1000x667.jpg');
INSERT INTO images VALUES (DEFAULT, 'generic news', 'https://cdn.ymaws.com/www.itsmfusa.org/resource/resmgr/images/more_images/news-3.jpg');
INSERT INTO images VALUES (DEFAULT, 'pepe hate', 'https://thumbnails.cbc.ca/maven_legacy/thumbnails/248/463/pepe_YT_frame_0.jpg');
INSERT INTO images VALUES (DEFAULT, 'need more ram', 'https://cdn.hswstatic.com/gif/add-ram-desktop-2-1.jpg');
INSERT INTO images VALUES (DEFAULT, 'stonks', 'https://i.kym-cdn.com/photos/images/newsfeed/001/499/826/2f0.png');
INSERT INTO images VALUES (DEFAULT, 'erna', 'https://ap.mnocdn.no/images/5fa3a4d8-f855-4d3e-a6bb-0fb7a7f0bfff?fit=crop&h=810&q=80&w=1440');
INSERT INTO images VALUES (DEFAULT, 'society', 'https://media.tenor.com/images/e0f1b1fd904c87214d3f82ad6f1fd283/tenor.png');

INSERT INTO category VALUES (DEFAULT, 'Nyheter');
INSERT INTO category VALUES (DEFAULT, 'Sport');
INSERT INTO category VALUES (DEFAULT, 'Kultur');
INSERT INTO category VALUES (DEFAULT, 'Musikk');

INSERT INTO articles VALUES (DEFAULT, 'Test artikkel', 'Dette er en test artikkel', 'Dette er en artikkel for å teste databasen.', 1, 10, 10, 1, 1, DEFAULT);
INSERT INTO articles VALUES (DEFAULT, 'newsfeed', 'Dette er en test artikkel', 'Dette er en artikkel for å teste databasen.', 1, 10, 10, 1, 2, DEFAULT);