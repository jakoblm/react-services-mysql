var mysql = require("mysql");
const NewsDao = require("../src/dao/NewsDao.js");
const runsqlfile = require("./runSQL.js");

// GitLab CI Pool
var pool = mysql.createPool({
	connectionLimit: 1,
	host: "mysql",
	user: "root",
	password: "",
	database: "jakoblm",
	debug: false,
	multipleStatements: true
});

let newsDao = new NewsDao(pool);

beforeAll(done => {
	runsqlfile("tests/setup.sql", pool, done);
});

afterAll(() => {
	pool.end();
});

// artikkel i db
// article_id   title           header                       content                                       image up down cat importace date
// DEFAULT,    'Test artikkel', 'Dette er en test artikkel', 'Dette er en artikkel for å teste databasen.',  1,   10, 10,  1,  1,       DEFAULT);

test("get one article from db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data=" + JSON.stringify(data)
		);
		expect(data.length).toBe(1);
		expect(data[0].article_id).toBe(1);
		expect(data[0].title).toBe("Test artikkel");
		done();
	}

	newsDao.getArticle(1, callback);
});

test("get frontpage from db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data=" + JSON.stringify(data)
		);
		expect(data.length).toBe(1);
		expect(data[0].importance).toBe(1);
		done();
	}

	newsDao.getArticlesByImportance(1, callback);
});

test("get Newsfeed from db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data=" + JSON.stringify(data)
		);
		expect(data.length).toBe(1);
		expect(data[0].importance).toBe(2);
		done();
	}

	newsDao.getArticlesByImportance(2, callback);
});

test("get first images from db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data=" + JSON.stringify(data)
		);
		expect(data.length).toBe(1);
		expect(data[0].image_name).toBe("Joker faller");
		done();
	}

	newsDao.getImageById(1, callback);
});

test("remove article from db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data.length=" + data.length
		);
		expect(data.affectedRows).toBe(1);
		done();
	}
	newsDao.deleteArticle(1, callback);
});

test("add article to db", done => {
	function callback(status, data) {
		console.log(
			"Test callback: status=" + status + ", data=" + JSON.stringify(data)
		);
		expect(data.affectedRows).toBeGreaterThanOrEqual(1);
		done();
	}

	//  [json.title, json.header, json.content, json.image_id,json.category_id, json.importance],
	newsDao.SubmitArticle(
		{title: "ny artikkel", header: "test header", content: "innhold", image_id: 1, category_id: 1, importance: 1 },
		callback)
});

