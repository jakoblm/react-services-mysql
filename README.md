# Den Lure Vind - Community nettavis

Dette er en community nettavis laget i faget TDAT2003 Systemutvikling 2 med web-applikasjoner

[![pipeline status](https://gitlab.stud.idi.ntnu.no/jakoblm/react-services-mysql/badges/master/pipeline.svg)](https://gitlab.stud.idi.ntnu.no/jakoblm/react-services-mysql/commits/master)

## Features

- Scrolling newsfeed som oppdateres hvert femte sekund med de 3 nyeste saker.
- Gjenbrukte komponenter og widgets.
- Twitter feed.
- Kommentarer
- Dao server.
- Klient services.
- Funksjonelle algortimer.
- continuous integration via Gitlab.
- MySQL database.
- Flow type checker.
- RESTful

# Forside

![alt text](https://i.imgur.com/a6Jdxio.jpg "Forside")

# Artikkel

![alt text](https://i.imgur.com/7KSMHJq.png "Artikkel")

# Kommentarfelt

![alt text](https://i.imgur.com/LVeO3lo.png "Kommentarfelt")

# Rediger og opprett sak

![alt text](https://i.imgur.com/qaPiGHV.png "Rediger sak")


## Client: run tests and start
From the top-level repository folder:
```sh
cd client
npm install
npm test
npm start
```

## Server: run tests and start
From the top-level repository folder:
```sh
cd server
npm install
npm test
npm start
```
